﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelevisaoCet30GUI
{
	
	public partial class Form1 : Form
	{
		private Tv minhaTV;
		public Form1()
		{
			InitializeComponent();
			minhaTV = new Tv();

			LabelStatus.Text = minhaTV.EnviaMensagem();

		}

		private void ButtonOnOff_Click(object sender , EventArgs e)
		{
			if( !minhaTV.GetEstado() )
			{
				minhaTV.LigaTv();
				LabelStatus.Text = minhaTV.EnviaMensagem();
			}
			else
			{
				minhaTV.DesligaTv();
				LabelStatus.Text = minhaTV.EnviaMensagem();
			}
		}
	}
}
