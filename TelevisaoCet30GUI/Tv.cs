﻿using System;
using System.IO;

namespace TelevisaoCet30GUI
{

	public class Tv
	{
		private bool estado;

		private int canal;

		private string mensagem;

		private int volume;


		public Tv()
		{
			estado = false;
			canal = 1;
			volume = 50;
			mensagem = "Nova tv criada com sucesso!";
		}

		public int GetCanal()
		{
			return canal;
		}

		public void MudaCanal(int canal)
		{
			this.canal = canal;
		}

		public string EnviaMensagem()
		{
			return mensagem;
		}

		public void LigaTv()
		{
			if( !estado )
			{
				estado = true;
				LerInfo();
				mensagem = "TV Ligada!";
			}
		}

		public void DesligaTv()
		{
			if( estado )
			{
				estado = false;
				GravarInfo();
				mensagem = "TV Desligada!";
			}
		}

		public void AumentaVolume(int valor)
		{
			volume += valor;
		}

		public void DiminuiVolume(int valor)
		{
			volume -= valor;
		}

		public int GetVolume()
		{
			return volume;
		}

		public bool GetEstado()
		{
			return estado;
		}
		private void GravarInfo()
		{
			string ficheiro = @"tvInfo.txt";

			string linha = canal + ";" + volume;

			StreamWriter sw = new StreamWriter(ficheiro , false);

			if( !File.Exists(ficheiro) )
			{
				sw = File.CreateText(ficheiro);
			}

			sw.WriteLine(linha);
			sw.Close();
		}

		private void LerInfo()
		{
			string ficheiro = @"tvInfo.txt";

			StreamReader sr;

			if( File.Exists(ficheiro) )
			{
				sr = File.OpenText(ficheiro);

				string linha = "";

				while( ( linha = sr.ReadLine() ) != null )
				{
					string[] campos = new string[2];

					campos = linha.Split(';');

					canal = Convert.ToInt32(campos[0]);
					volume = Convert.ToInt32(campos[1]);
				}
				sr.Close();
			}
		}
	}



}
