﻿namespace TelevisaoCet30GUI
{
	partial class Form1
	{
		/// <summary>
		/// Variável de designer necessária.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpar os recursos que estão sendo usados.
		/// </summary>
		/// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
		protected override void Dispose(bool disposing)
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código gerado pelo Windows Form Designer

		/// <summary>
		/// Método necessário para suporte ao Designer - não modifique 
		/// o conteúdo deste método com o editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.LabelStatus = new System.Windows.Forms.Label();
			this.ButtonOnOff = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// LabelStatus
			// 
			this.LabelStatus.AutoSize = true;
			this.LabelStatus.Location = new System.Drawing.Point(131, 188);
			this.LabelStatus.Name = "LabelStatus";
			this.LabelStatus.Size = new System.Drawing.Size(35, 13);
			this.LabelStatus.TabIndex = 0;
			this.LabelStatus.Text = "label1";
			// 
			// ButtonOnOff
			// 
			this.ButtonOnOff.Location = new System.Drawing.Point(369, 188);
			this.ButtonOnOff.Name = "ButtonOnOff";
			this.ButtonOnOff.Size = new System.Drawing.Size(75, 23);
			this.ButtonOnOff.TabIndex = 1;
			this.ButtonOnOff.Text = "button1";
			this.ButtonOnOff.UseVisualStyleBackColor = true;
			this.ButtonOnOff.Click += new System.EventHandler(this.ButtonOnOff_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(711, 362);
			this.Controls.Add(this.ButtonOnOff);
			this.Controls.Add(this.LabelStatus);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LabelStatus;
		private System.Windows.Forms.Button ButtonOnOff;
	}
}

